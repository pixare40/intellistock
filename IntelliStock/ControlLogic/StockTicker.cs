﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR;
using IntelliStock.Hubs;
using System.Timers;
using IntelliStock.BusinessLayer.Interfaces;
using IntelliStock.BusinessLayer;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using IntelliStock.Statistics;
using IntelliStock.Statistics.Interfaces;

namespace IntelliStock.ControlLogic
{
    public class StockTicker
    {
        //Create Singleton Instance of class
        private readonly static Lazy<StockTicker> _instance = new Lazy<StockTicker>(() => new StockTicker(GlobalHost.ConnectionManager.GetHubContext<StockTickerHub>().Clients));
        IDataFetcher _datafetcher;
        StatisticsService statisticsservice;

        public StockTicker(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
            _datafetcher = new DataFetcher();
            statisticsservice = StatisticsService.GetInstance();
        }
        int currentDate = 1;
        int CurrentDate
        {
            get
            {
                return currentDate;
            }
            set
            {
                currentDate = value;
            }
        }

        public static StockTicker Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        #region Stock Methods

        public void GetStockData(string symbol)
        {
            var result = _datafetcher.GetSingleQuote(symbol);
            Debug.WriteLine("Fetching Yahoo Data for " + symbol);
            Clients.All.AddStockToPage(result.Symbol, result.AskRealTime, result.BidRealTime, result.Currency);
        }

        public void GetHistoricalQuotes(string symbol)
        {
            var quotedata = _datafetcher.GetHistoricalQuotes(symbol);

            var json = JsonConvert.SerializeObject(quotedata, new JavaScriptDateTimeConverter());
            //Debug.WriteLine(json);
            var jsondata = JsonConvert.DeserializeObject(json);
            //Debug.WriteLine(jsondata);

            Clients.All.DrawQuoteGraph(jsondata);
            
        }

        public void InitializeData()
        {
            var initalized = _datafetcher.InitializeData();
            if (initalized)
                Clients.All.DataInitialized();
            else
                Clients.All.ErrorOccurred();
        }

        internal void CalculateMA(string symbol, string shortmoving, string longmoving)
        {
            var data = (from d in _datafetcher.GetHistoricalQuotes(symbol)
                        orderby d.Date ascending
                        select d).ToList();

            var shortarray = statisticsservice.CalculateMovingAverage(data, Convert.ToInt32(shortmoving));
            var longarray = statisticsservice.CalculateMovingAverage(data, Convert.ToInt32(longmoving));

            //foreach (var d in shortarray)
            //{
            //    Debug.Write(d+" ");
            //}

            //foreach (var d in longarray)
            //{
            //    Debug.Write(d + " ");
            //}

            var jsonshort = JsonConvert.SerializeObject(shortarray);
            var jsonlong = JsonConvert.SerializeObject(longarray);

            Clients.All.RenderResults(jsonshort, jsonlong);
        }
        #endregion




        internal void CalculatePnL(string symbol, string shortmoving, string longmoving, string amountInvested)
        {
            var data = (from d in _datafetcher.GetHistoricalQuotes(symbol)
                        orderby d.Date ascending
                        select d).ToList();
            var shortarray = statisticsservice.CalculateMovingAverage(data, Convert.ToInt32(shortmoving));
            var longarray = statisticsservice.CalculateMovingAverage(data, Convert.ToInt32(longmoving));

            var dividendresult = statisticsservice.CalculatePnL(data, shortarray, longarray, Convert.ToDouble(amountInvested));

            Clients.All.RenderPnL(dividendresult.SharesHeld, dividendresult.InvestmentReturn);
        }

        internal void RunStrategy(string strategy)
        {
            var strategysuccesful = statisticsservice.RunStrategy(strategy);
            if (strategysuccesful)
            {
                if (strategy == "Clenow")
                    Clients.All.RenderClenowResults();
                else if (strategy == "CRSI")
                    Clients.All.RenderCRSIResults();
            }
            else
            {
                Clients.All.ErrorOccurred();
                Debug.WriteLine("An Error Occured");
            }
        }
    }
}