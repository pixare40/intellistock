﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR;
using IntelliStock.ControlLogic;
using IntelliStock.BusinessLayer;
using IntelliStock.Data;
using IntelliStock.BusinessLayer.Interfaces;

namespace IntelliStock.Hubs
{
    public class StockTickerHub : Hub
    {
        private readonly StockTicker _stockTicker;

        public StockTickerHub():
            this(StockTicker.Instance)
        {

        }

        public StockTickerHub(StockTicker stockticker)
        {
            _stockTicker = stockticker;
        }

        public void GetStockData(string symbol)
        {
            _stockTicker.GetStockData(symbol);
        }

        public void GetHistoricalData(string symbol)
        {
            _stockTicker.GetHistoricalQuotes(symbol);
        }

        public void InitializeHistoricalData()
        {
            _stockTicker.InitializeData();
        }

        public void CalculateMA(string symbol, string shortmoving, string longmoving)
        {
            _stockTicker.CalculateMA(symbol, shortmoving, longmoving);
        }

        public void CalculatePnL(string symbol, string shortmoving, string longmoving, string amountInvested)
        {
            _stockTicker.CalculatePnL(symbol, shortmoving, longmoving, amountInvested);
        }

        public void RunStrategy(string strategy)
        {
            _stockTicker.RunStrategy(strategy);
        }
    }
}