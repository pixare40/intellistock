﻿$(function () {
    var stocksticker = $.connection.stockTickerHub;
    stocksticker.client.addStockToPage = function (symbol, ask, bid, currency) {
        $('#stocks').append('<li><strong>' + htmlEncode(symbol)
            + '</strong>: ' + htmlEncode(ask) +' '+ htmlEncode(bid) + htmlEncode(currency) + '</li>');
    };

    var fixDate = function (dateIn) {
        var dat = new Date(dateIn);
        return Date.UTC(dat.getFullYear(), dat.getMonth(), dat.getDate());
    }

    stocksticker.client.dataInitialized = function () {
        $('#loading').addClass('hidden');
        $('#datainitialized').removeClass('hidden');
    }

    stocksticker.client.errorOccurred = function () {
        $('#loading').addClass('hidden');
        $('#error').removeClass('hidden');
    }

    stocksticker.client.drawMyGraphs = function () {
        $('#loading').addClass('hidden');
        $('#plot').removeClass('hidden');
    }

    stocksticker.client.renderResults = function (shortarray, longarray) {
        console.log('Result received');
        $('#loading').addClass('hidden');
        $('#results').append('Short Array: ' + htmlEncode(shortarray) + '<br/>');
        $('#results').append('Long Array: ' + htmlEncode(longarray) + '<br/>');
    }

    stocksticker.client.renderClenowResults = function () {
        console.log('Clenow Result received');
        $('#loadingarea').text('');
        $('#clenowresults').removeClass('hidden');
    }

    stocksticker.client.renderCRSIResults = function () {
        console.log('Clenow Result received');
        $('#loadingarea').text('');
        $('#crsiresults').removeClass('hidden');
    }

    stocksticker.client.renderPnL = function (shares, investmentreturn) {
        console.log('PnL received');
        $('#loading').addClass('hidden');
        $('#returns').append('Shares Held: <h1>' + htmlEncode(shares) + '</h1> <br/>');
        $('#returns').append('Investment Return: ' + htmlEncode(investmentreturn) + ' in funds <br/>');
    }

    stocksticker.client.updateQuoteGraph = function (date, open, high, low, close, volume) {

        var thechart = $('#quotechart').highcharts();
        var seriesOHLC = thechart.series[0];
        var seriesVolume = thechart.series[1];

        seriesOHLC.addPoint([fixDate(date), open, high, low, close], false, true);
        seriesVolume.addPoint([fixDate(date), volume], false, true);

        console.log(seriesOHLC.name);
        console.log(seriesVolume.name);

        thechart.redraw();
    }

    stocksticker.client.drawQuoteGraph = function (data) {
        $('#loading').addClass('hidden');

        var symbol = data[1].Symbol;

        console.log(fixDate(data[1].Date));
        var ohlc = [],
        volume = [],
        dataLength = Object.keys(data).length,
        // set the allowed units for data grouping
        groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]],

        i = 0;

        for (i; i < dataLength; i += 1) {
            ohlc.push([
                fixDate(data[i].Date), // the date
                data[i].Open, // open
                data[i].High, // high
                data[i].Low, // low
                data[i].Close // close
            ]);

            volume.push([
                fixDate(data[i].Date), // the date
                data[i].Volume // the volume
            ]);
        }

        
        // create the chart
       $('#quotechart').highcharts('StockChart', {

           

            rangeSelector: {
                selected: 1
            },

            title: {
                text: symbol + ' Historical'
            },

            yAxis: [{
                labels: {
                    align: 'right',
                    x: -3
                },
                title: {
                    text: 'OHLC'
                },
                height: '60%',
                lineWidth: 2
            }, {
                labels: {
                    align: 'right',
                    x: -3
                },
                title: {
                    text: 'Volume'
                },
                top: '65%',
                height: '35%',
                offset: 0,
                lineWidth: 2
            }],

            series: [{
                type: 'candlestick',
                name: symbol,
                id: '1',
                data: ohlc,
                dataGrouping: {
                    units: groupingUnits
                }
            }, {
                type: 'column',
                name: 'Volume',
                id:'2',
                data: volume,
                yAxis: 1,
                dataGrouping: {
                    units: groupingUnits
                }
            }]
        });
    };

    $.connection.hub.start().done(function () {
        $('#sendrequest').click(function () {
            stocksticker.server.getStockData($('#symbol').val().toUpperCase());
        });

        $('#gethistoricaldata').click(function () {
            $('#loading').removeClass('hidden');
            stocksticker.server.getHistoricalData($('#symbol').val().toUpperCase());
        });

        $('#initializedata').click(function () {
            $('#loading').removeClass('hidden');
            stocksticker.server.initializeHistoricalData();
        });

        $('#getma').click(function () {
            console.log('Get MA clicked');
            $('#loading').removeClass('hidden');
            stocksticker.server.calculateMA($('#masymbol').val(), $('#shortmoving').val(), $('#longmoving').val());
        });

        $('#pnl').click(function () {
            console.log('PNL clicked');
            $('#loading').removeClass('hidden');
            stocksticker.server.calculatePnL($('#masymbol').val(), $('#shortmoving').val(), $('#longmoving').val(), $('#investment').val());
        });

        $('#showgraph').click(function () {
            console.log('show graph clicked');
            $('#loading').removeClass('hidden');
            stocksticker.server.showGraph();
        });
        $('#clenow').click(function () {
            console.log('clenow clicked');
            $('#loadingarea').text('Running Clenow Trend Following, Please Wait...');
            stocksticker.server.runStrategy("Clenow");
        });
        $('#connorsrsi').click(function () {
            console.log('crsi clicked');
            $('#loadingarea').text('Running ConnorsRSI Please Wait...');
            stocksticker.server.runStrategy("CRSI");
        });
    })
});
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}