﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IntelliStock.Data;
using IntelliStock.BusinessLayer;
using IntelliStock.BusinessLayer.Interfaces;
using IntelliStock.Statistics.Interfaces;

namespace IntelliStock.Controllers
{
    public class HomeController : Controller
    {

        IDataFetcher datafetcher;
        IPersistence persistence;
        //IStatisticsService statisticsservice;

        public HomeController(IDataFetcher datafetcher, IPersistence persistence)
        {
            this.datafetcher = datafetcher;
            this.persistence = persistence;
            //this.statisticsservice = statisticsservice;

        }

        public ActionResult Index()
        {
            //double[] data = new double[] { 30, 40, 20, 10, 40, 20, 50, 20, 10, 40, 20, 30, 10, 40, 10, 20, 30, 40, 20, 10, 40, 20, 50, 20, 10, 40, 20, 30, 10, 40, 10, 20 };
            //statisticsservice.CalculateMovingAverage(data, 5);
            //statisticsservice.CalculateMovingAverage(data, 10);
            //persistence.HistoricalDataInitialized();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Application description";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult MovingAverageAnalysis()
        {

            return View();
        }

        public ActionResult StrategyAnalysis()
        {
            return View();
        }
    }
}