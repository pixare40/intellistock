﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using IntelliStock;
using IntelliStock.Data;
using IntelliStock.BusinessLayer;
using System.Web.Mvc;
using IntelliStock.BusinessLayer.Interfaces;
using IntelliStock.Statistics.Interfaces;
using IntelliStock.Statistics;

namespace IntelliStock
{
    public class Bootstrap
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //here I register my containers for dependency injection
            container.RegisterType<IDataFetcher, DataFetcher>();
            container.RegisterType<IPersistence, Persistence>();
            //container.RegisterType<IStatisticsService, StatisticsService>();

            RegisterTypes(container);
            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }
    }
}