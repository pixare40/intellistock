﻿using IntelliStock.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.Statistics.Interfaces
{
    public interface IStatisticsService
    {
        IStatisticsService getInstance();
        double[] CalculateMovingAverage(IList<HistoricalQuote> data, int duration);
        Dividend CalculatePnL(IList<HistoricalQuote> data, double[] shortMA, double[] longMA, double amountInvested);
    }
}
