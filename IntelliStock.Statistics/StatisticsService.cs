﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelliStock.Statistics.Interfaces;
using RDotNet;
using System.Diagnostics;
using IntelliStock.Data;
using IntelliStock.Data.Models;
using System.IO;

namespace IntelliStock.Statistics
{
    public class StatisticsService
    {
       // private static IPersistence persistence;
        public REngine _engine;
        public static StatisticsService _instance;
        string clenowfilepath;
        string crsifilepath;

        private StatisticsService()
        {
            _engine = InitializeR();
            clenowfilepath = "C:\\Users\\Kabaji Egara\\Source\\Repos\\intellistock\\IntelliStock\\RWD\\Clenow.bat";
            crsifilepath = "C:\\Users\\Kabaji Egara\\Source\\Repos\\intellistock\\IntelliStock\\RWD\\CummulativeConnorsRSI.bat";
        }

        public static StatisticsService GetInstance()
        {
            if (_instance == null)
            {
                _instance = new StatisticsService();
                return _instance;
            }

            else
            {
                return _instance;
            }
        }
        private REngine InitializeR()
        {
            //Check if Engine is not null
            if (_engine == null) 
            {
                //Initialize R Engine
                REngine.SetEnvironmentVariables();
                REngine engine = REngine.GetInstance();

                //Set Workspace directory variables
                //engine.Evaluate("wdirectory <- 'C:/Users/kegara/Documents/Visual Studio Projects/IntelliStock/IntelliStock/RWD'");
                //engine.Evaluate("setwd(wdirectory)");

                //Set CRAN  Mirror to use
                //engine.Evaluate("options(repos=structure(c(CRAN='http://cran.ma.imperial.ac.uk/')))");

                ////Ensure necessary packages are installed 
                //engine.Evaluate("install.packages('blotter', repos='http://R-Forge.R-project.org')");
                //engine.Evaluate("install.packages('FinancialInstrument', repos='http://R-Forge.R-project.org')");
                //engine.Evaluate("install.packages('quantstrat', repos='http://R-Forge.R-project.org')");
                //engine.Evaluate("install.packages('foreach')");

                ////Set Requirement
                //engine.Evaluate("require(quantstrat)");
                //Debug.WriteLine("Quantstrat initialized");
                //engine.Evaluate("require(PerformanceAnalytics)");
                //Debug.WriteLine("Performance Analytics initialized");

                _engine = engine;
                Debug.WriteLine("R Engine Loaded");
                return engine;
            }
            else
            {
                return _engine;
            }
           
        }


        
        //public void TestR()
        //{
        //    var engine = InitializeR();
        //    CharacterVector charVec = engine.CreateCharacterVector(new[] { "Hello, R world!, .NET speaking" });
        //    engine.SetSymbol("greetings", charVec);
        //    engine.Evaluate("str(greetings)"); // print out in the console
        //    string[] a = engine.Evaluate("'Hi there .NET, from the R engine'").AsCharacter().ToArray();
        //    Debug.WriteLine("R answered: '{0}'" + a[0]);
        //    Debug.WriteLine("Press any key to exit the program");
        //    engine.Dispose();
        //}

        public double[] CalculateMovingAverage(IList<HistoricalQuote> data,int duration)
        {
            var engine = InitializeR();
            var arraydata = (from d in data
                             select d.Close).ToArray();
            NumericVector statdata = engine.CreateNumericVector(arraydata);
            IntegerVector _duration = engine.CreateInteger(duration);
            engine.SetSymbol("statdata", statdata);
            engine.SetSymbol("duration", _duration);

            engine.Evaluate("library(TTR)");
            var ma = engine.Evaluate("ma<-SMA(statdata,n=duration)").AsNumeric();
            //engine.Dispose();
            Double[] maArray = ma.ToArray();

            //maArray = maArray.Where(val => !Double.IsNaN(val)).ToArray();
            Debug.WriteLine(maArray.Count());
            for (int i = 0; i < maArray.Count(); i++)
            {
                maArray[i] = Math.Round(maArray[i], 1);
                Debug.WriteLine(maArray[i]);
            }
            
            return maArray;
        }

        public Dividend CalculatePnL(IList<HistoricalQuote> data, double[] shortMA, double[] longMA, double amountInvested)
        {
            double returnAmount = amountInvested;
            var instrumentdata = (from n in data
                                  select n.Close).ToArray();
            int sharesbought = 0;
            for(int i = 0; i<shortMA.Length; i++)
            {
                if (shortMA[i] == longMA[i])
                {
                    if (!Double.Equals(shortMA[i], Double.NaN) && !Double.Equals(longMA[i], Double.NaN))
                    {
                        if (shortMA[i + 1] > longMA[i + 1])
                        {
                            sharesbought = (int)(amountInvested / instrumentdata[i]);
                            returnAmount = amountInvested % instrumentdata[i];
                        }
                        else if (shortMA[i + 1] < longMA[i + 1])
                        {
                            returnAmount += (sharesbought * instrumentdata[i]);
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            
            return new Dividend(sharesbought, returnAmount);
        }

        public bool RunStrategy(string strategy)
        {
            if (strategy == "Clenow")
            {
                return RunBackTest(clenowfilepath);
            }
            else if (strategy == "CRSI")
            {
                return RunBackTest(crsifilepath);
            }
            else
            {
                return false;
            }
            
            
        }

        private bool RunBackTest(string stratfilepath)
        {
            try
            {
                string filepath = stratfilepath;
                ProcessStartInfo psi = new ProcessStartInfo("cmd.exe");
                psi.UseShellExecute = false;
                psi.WorkingDirectory = "C:\\Users\\Kabaji Egara\\Source\\Repos\\intellistock\\IntelliStock\\RWD\\";
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;

                //start the process
                Process proc = Process.Start(psi);

                //Open BAT file for reading
                StreamReader strm = File.OpenText(filepath);

                //Attach Output for reading
                StreamReader sOut = proc.StandardOutput;

                //Attach the in for writing
                StreamWriter sIn = proc.StandardInput;

                //Write each line of the batch file to standard input
                while (strm.Peek() != -1)
                {
                    sIn.WriteLine(strm.ReadLine());
                }

                strm.Close();

                //Exit CMD.EXE
                string stEchoFmt = "#{0} run successfully. Exiting";

                sIn.WriteLine(String.Format(stEchoFmt, filepath));
                sIn.WriteLine("EXIT");

                //close the process
                proc.Close();

                // Read the sOut to a string.
                string results = sOut.ReadToEnd().Trim();

                // Close the io Streams;
                sIn.Close();
                sOut.Close();

                Debug.WriteLine("Written to output");


                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
        }


    }
}
