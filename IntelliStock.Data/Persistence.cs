﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelliStock.Data.Models;
using System.Diagnostics;
using System.Configuration;

namespace IntelliStock.Data
{
    public class Persistence: IPersistence
    {
        IntelliStockContext _context;
        public Persistence()
        {
            _context = new IntelliStockContext();
        }

        private IntelliStockContext Context
        {
            get
            {
                return _context;
            }
        }
        public bool SaveHistoricalQuote(Models.HistoricalQuote quote)
        {
            try
            {
                Context.HistoricalQuote.Add(quote);
                Context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Saving Historical Data failed" + e.Message);
                Debug.WriteLine(e.InnerException);
                return false;
            }
            
        }

        public bool SaveHistoricalQuote(IList<HistoricalQuote> data)
        {
            try
            {
                Context.HistoricalQuote.AddRange(data);
                Context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Saving Historical Data failed" + e.Message);
                Debug.WriteLine(e.InnerException);
                return false;
            }
        }

        public IList<Models.HistoricalQuote> GetHistoricalQuotes(string symbol)
        {
            var historicalquotes = (from data in Context.HistoricalQuote
                                    where data.Symbol == symbol
                                    orderby data.Date ascending
                                    select data).ToList();
            Debug.WriteLine(historicalquotes.Count);
            return historicalquotes;
        }

        public bool SaveCurrentQuote(Models.CurrentQuote quote)
        {
            try
            {
                Context.CurrentQuote.Add(quote);
                Context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Saving Current Data failed" + e.Message);
                Debug.WriteLine(e.InnerException);
                return false;
            }
        }

        public bool HistoricalDataInitialized()
        {
            string datainitialized = ConfigurationManager.AppSettings["HistoricalDataInitialized"];
            if (datainitialized == "true")
            {
                Debug.WriteLine("Application Data Initilized");
                return true;
            }
            else if (datainitialized == "false")
            {
                Debug.WriteLine("Application Data Not Initilized");
                return false;
            }
            else
            {
                Debug.WriteLine("Unknown Error");
                return false;
            }
        }

        public void InitializeHistoricalData()
        {
            ConfigurationManager.AppSettings.Set("HistoricalDataInitialized", "true");
        }


        public IList<HistoricalQuote> GetHistoricalQuotes(string symbol, DateTime startdate, DateTime enddate)
        {
            var data = (from hquotes in Context.HistoricalQuote
                        where hquotes.Symbol == symbol
                        orderby hquotes.Date ascending
                        select hquotes).ToList();
            return data;
        }
    }
}
