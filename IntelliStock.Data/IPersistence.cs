﻿using IntelliStock.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.Data
{
    public interface IPersistence
    {
        Boolean SaveHistoricalQuote(HistoricalQuote quote);
        bool SaveHistoricalQuote(IList<HistoricalQuote> data);
        IList<HistoricalQuote> GetHistoricalQuotes(string symbol);

        IList<HistoricalQuote> GetHistoricalQuotes(string symbol, DateTime startdate, DateTime enddate);
        Boolean SaveCurrentQuote(CurrentQuote quote);
        bool HistoricalDataInitialized();

        void InitializeHistoricalData();
    }
}
