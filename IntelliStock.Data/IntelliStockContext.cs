﻿using IntelliStock.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.Data
{
    public class IntelliStockContext : DbContext
    {
        public IntelliStockContext()
            : base()
        {

        }

        public DbSet<HistoricalQuote> HistoricalQuote { get; set; }
        public DbSet<CurrentQuote> CurrentQuote { get; set; }
    }
}
