﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.Data.Models
{
    public class Dividend
    {
        public Dividend(int sharesheld, double investmentreturn)
        {
            SharesHeld = sharesheld;
            InvestmentReturn = investmentreturn;
        }
        public int SharesHeld
        {
            get;
            private set;
        }

        public double InvestmentReturn
        {
            get;
            private set;
        }
    }
}
