﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.Data.Models
{
    public class CurrentQuote
    {
        public CurrentQuote()
        {

        }

       
        public int CurrentQuoteID { get; set; }
        public string Symbol { get; set; }
        public double AskRealTime { get; set; }
        public double BidRealTime { get; set; }
        public DateTime LastTradeDate { get; set; }
        public string Currency { get; set; }
    }
}
