﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.Data.Models
{
    public class Quote
    {
        public Quote()
        {

        }
        public string Symbol { get; set; }
        public double AverageDailyVolume { get; set; }
        public double Change { get; set; }
        public double DaysLow { get; set; }
        public double DaysHigh { get; set; }
        public double YearLow { get; set; }
        public double YearHigh { get; set; }
        public string MarketCap { get; set; }
        public double LastTradePrice { get; set; }
        public string DaysRange { get; set; }
        public string Name { get; set; }
        public double Volume { get; set; }
        public string StockExchange { get; set; }

    }
}
