﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.Data.Models
{
    public class HistoricalQuote
    {
        public HistoricalQuote()
        {

        }
        public int HistoricalQuoteID { get; set; }
        public string Symbol { get; set; }
        public DateTime Date { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public double Volume { get; set; }
        public double Adj_Close { get; set; }
    }
}
