﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliStock.BusinessLayer.Interfaces
{
     public interface IAdapter
    {
         void Initialize();
    }
}
