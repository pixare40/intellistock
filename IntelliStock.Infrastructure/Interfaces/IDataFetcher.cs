﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelliStock.Data.Models;

namespace IntelliStock.BusinessLayer.Interfaces
{
    public interface IDataFetcher
    {
        CurrentQuote GetSingleQuote(string symbol);

        IList<HistoricalQuote> GetAPIQuotes(string symbol, string startDate, string endDate);

        IList<HistoricalQuote> GetHistoricalQuotes(string symbol);

        bool InitializeData();



    }
}
