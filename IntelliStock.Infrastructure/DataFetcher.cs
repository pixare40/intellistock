﻿using IntelliStock.BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelliStock.Data.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Diagnostics;
using IntelliStock.Data;

namespace IntelliStock.BusinessLayer
{
    public class DataFetcher : IDataFetcher
    {
        IPersistence _persistence;

        public DataFetcher()
        {
            _persistence = new Persistence();
        }
        public CurrentQuote GetSingleQuote(string symbol)
        {
            StringBuilder datasource = new StringBuilder();
            datasource.Append("http://query.yahooapis.com/v1/public/yql?q=");
            datasource.Append(Uri.EscapeUriString("select * from yahoo.finance.quotes where symbol = '"+symbol+"'"));
            datasource.Append("&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

            string results = "";

            using (WebClient wc = new WebClient())
            {
                results = wc.DownloadString(datasource.ToString());
            }

            JObject dataObject = JObject.Parse(results);
            JObject jsonArray = (JObject)dataObject["query"]["results"]["quote"];

            var quoteObject = new CurrentQuote();

            quoteObject.Symbol = (string)jsonArray["symbol"];
            quoteObject.AskRealTime = Convert.ToDouble(jsonArray["Ask"]);
            quoteObject.BidRealTime = Convert.ToDouble(jsonArray["Bid"]);
            quoteObject.Currency = (string)jsonArray["Currency"];
            quoteObject.LastTradeDate = Convert.ToDateTime(jsonArray["LastTradeDate"]);

            Debug.WriteLine("Fetched and sent content");

            return quoteObject;
        }

        public IList<HistoricalQuote> GetAPIQuotes(string symbol, string startDate, string endDate)
        {
            StringBuilder datasource = new StringBuilder();
            datasource.Append("http://query.yahooapis.com/v1/public/yql?q=");
            datasource.Append(Uri.EscapeUriString("select * from yahoo.finance.historicaldata where symbol = '"+symbol+"' and startDate = '"+startDate+"' and endDate = '"+endDate+"'"));
            datasource.Append("&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

             string results = "";

            using (WebClient wc = new WebClient())
            {
                results = wc.DownloadString(datasource.ToString());
            }

            JObject dataObject = JObject.Parse(results);
            JArray jsonArray = (JArray)dataObject["query"]["results"]["quote"];

            var data = new List<HistoricalQuote>();

            foreach (var dataresult in jsonArray)
            {
                var quoteobject = new HistoricalQuote();
                quoteobject.Symbol = (String)dataresult["Symbol"];
                quoteobject.Date = Convert.ToDateTime(dataresult["Date"]);
                quoteobject.Open = Convert.ToDouble(dataresult["Open"]);
                quoteobject.High = Convert.ToDouble(dataresult["High"]);
                quoteobject.Low = Convert.ToDouble(dataresult["Low"]);
                quoteobject.Close = Convert.ToDouble(dataresult["Close"]);
                quoteobject.Volume = Convert.ToDouble(dataresult["Volume"]);
                quoteobject.Adj_Close = Convert.ToDouble(dataresult["Adj_Close"]);

                data.Add(quoteobject);
            }

            return data;
        }

        public IList<HistoricalQuote> GetHistoricalQuotes(string symbol)
        {
            return _persistence.GetHistoricalQuotes(symbol);
        }

        public bool InitializeData()
        {
            try 
            {
                if (!_persistence.HistoricalDataInitialized())
                {
                    var microsoftdata = GetAPIQuotes("MSFT", DateTime.Now.Subtract(new TimeSpan(365,0,0,0)).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
                    _persistence.SaveHistoricalQuote(microsoftdata);
                    var googledata = GetAPIQuotes("GOOG", DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
                    _persistence.SaveHistoricalQuote(googledata);
                    var alibabadata = GetAPIQuotes("BABA", DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
                    _persistence.SaveHistoricalQuote(alibabadata);
                    var appledata = GetAPIQuotes("AAPL", DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
                    _persistence.SaveHistoricalQuote(appledata);
                    var fbdata = GetAPIQuotes("FB", DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
                    _persistence.SaveHistoricalQuote(fbdata);
                    var ciscodata = GetAPIQuotes("CSCO", DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
                    _persistence.SaveHistoricalQuote(ciscodata);
                    var amicustherapeuticsdata = GetAPIQuotes("FOLD", DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
                    _persistence.SaveHistoricalQuote(amicustherapeuticsdata);

                    _persistence.InitializeHistoricalData();
                    _persistence.HistoricalDataInitialized();
                    return true;
                }
                else
                    return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("An exception occurred" + e.Message);
                return false;
            }
            
        }
    }
}
