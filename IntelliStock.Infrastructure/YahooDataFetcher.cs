﻿using IntelliStock.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelliStock.Data.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Diagnostics;
using IntelliStock.Data;

namespace IntelliStock.Infrastructure
{
    public class YahooDataFetcher : IDataFetcher
    {
        IPersistence _persistence;

        public YahooDataFetcher()
        {
            _persistence = new Persistence();
        }
        public CurrentQuote GetQuote(string symbol)
        {
            StringBuilder datasource = new StringBuilder();
            datasource.Append("http://query.yahooapis.com/v1/public/yql?q=");
            datasource.Append(Uri.EscapeUriString("select * from yahoo.finance.quotes where symbol = '"+symbol+"'"));
            datasource.Append("&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

            string results = "";

            using (WebClient wc = new WebClient())
            {
                results = wc.DownloadString(datasource.ToString());
            }

            JObject dataObject = JObject.Parse(results);
            JObject jsonArray = (JObject)dataObject["query"]["results"]["quote"];

            var quoteObject = new CurrentQuote();

            quoteObject.Symbol = (string)jsonArray["symbol"];
            quoteObject.AskRealTime = Convert.ToDouble(jsonArray["Ask"]);
            quoteObject.BidRealTime = Convert.ToDouble(jsonArray["Bid"]);
            quoteObject.Currency = (string)jsonArray["Currency"];
            quoteObject.LastTradeDate = Convert.ToDateTime(jsonArray["LastTradeDate"]);

            //quoteObject.Symbol = (String)jsonArray["Symbol"];
            //quoteObject.AverageDailyVolume = Convert.ToDouble(jsonArray["AverageDailyVolume"]);
            //quoteObject.Change = Convert.ToDouble(jsonArray["Change"]);
            //quoteObject.DaysLow = Convert.ToDouble(jsonArray["DaysLow"]);
            //quoteObject.DaysHigh = Convert.ToDouble(jsonArray["DaysHigh"]);
            //quoteObject.YearLow = Convert.ToDouble(jsonArray["YearLow"]);
            //quoteObject.YearHigh = Convert.ToDouble(jsonArray["YearHigh"]);
            //quoteObject.MarketCap = (String)jsonArray["MarketCapitalization"];
            //quoteObject.LastTradePrice = Convert.ToDouble(jsonArray["LastTradePriceOnly"]);
            //quoteObject.DaysRange = (string)jsonArray["DaysRange"];
            //quoteObject.Name = (string)jsonArray["Name"];
            //quoteObject.Volume = Convert.ToDouble(jsonArray["Volume"]);
            //quoteObject.StockExchange = (string)jsonArray["StockExchange"];

            Debug.WriteLine("Fetched and sent content");

            _persistence.SaveCurrentQuote(quoteObject);

            return quoteObject;
        }

        public IList<HistoricalQuote> GetQuotes(string symbol, string startDate, string endDate)
        {
            StringBuilder datasource = new StringBuilder();
            datasource.Append("http://query.yahooapis.com/v1/public/yql?q=");
            datasource.Append(Uri.EscapeUriString("select * from yahoo.finance.historicaldata where symbol = '"+symbol+"' and startDate = '"+startDate+"' and endDate = '"+endDate+"'"));
            datasource.Append("&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

             string results = "";

            using (WebClient wc = new WebClient())
            {
                results = wc.DownloadString(datasource.ToString());
            }

            JObject dataObject = JObject.Parse(results);
            JArray jsonArray = (JArray)dataObject["query"]["results"]["quote"];

            var data = new List<HistoricalQuote>();

            foreach (var dataresult in jsonArray)
            {
                var quoteobject = new HistoricalQuote();
                quoteobject.Symbol = (String)dataresult["Symbol"];
                quoteobject.Date = Convert.ToDateTime(dataresult["Date"]);
                quoteobject.Open = Convert.ToDouble(dataresult["Open"]);
                quoteobject.High = Convert.ToDouble(dataresult["High"]);
                quoteobject.Low = Convert.ToDouble(dataresult["Low"]);
                quoteobject.Close = Convert.ToDouble(dataresult["Close"]);
                quoteobject.Volume = Convert.ToDouble(dataresult["Volume"]);
                quoteobject.Adj_Close = Convert.ToDouble(dataresult["Adj_Close"]);

                if(!_persistence.HistoricalDataInitialized())
                _persistence.SaveHistoricalQuote(quoteobject);

                data.Add(quoteobject);
            }

            data.Reverse();
            return data;
        }




        public void InitializeData()
        {
            throw new NotImplementedException();
        }
    }
}
