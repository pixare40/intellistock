﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelliStock.BusinessLayer.Interfaces;
using IntelliStock.Statistics.Interfaces;
using IntelliStock.Statistics;
using IntelliStock.Data.Models;

namespace IntelliStock.BusinessLayer
{
    public class StatisticsAdapter : IAdapter
    {
        private static StatisticsService _statisticsService;

        public StatisticsAdapter()
        {
            Initialize();
        }
        public void Initialize()
        {
            _statisticsService = StatisticsService.GetInstance();
        }

        public StatisticsService StatisticsService
        {
            get
            {
                return _statisticsService;
            }
        }

        public double[] CalculateMovingAverage(IList<HistoricalQuote> data, int duration)
        {
            return StatisticsService.CalculateMovingAverage(data, duration);
        }

        public Dividend CalculatePnL(IList<HistoricalQuote> data, double[] shortMA, double[] longMA, double amountInvested)
        {
            return StatisticsService.CalculatePnL(data, shortMA, longMA, amountInvested);
        }
    }
}
